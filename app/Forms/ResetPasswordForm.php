<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ResetPasswordForm extends Form
{
    protected $clientValidationEnabled = false;

    protected $formOptions = [
        'method' => 'POST',
        'class' => 'form form-auth-resetpassword'
    ];

    public function buildForm()
    {
        $this->add('token', 'hidden', [
            'value' => $this->getData('token')
        ])
        ->add('email', 'email', [
            'attr' => ['class' => 'input'],
            'label' => __('Email'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-envelope']
        ])
        ->add('password', 'password', [
            'attr' => ['class' => 'input'],
            'label' => __('Password'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-key'],
        ])
        ->add('password_confirmation', 'password', [
            'attr' => ['class' => 'input'],
            'label' => __('Confirm password'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-key']
        ])
        ->add('submit', 'submit', [
            'attr' => ['class' => 'button is-link'],
            'wrapper' => ['class' => 'field'],
            'label' => '<span class="icon"><i class="fas fa-check"></i></span><span>'.__('Reset my password now').'</span>'
        ]);
    }
}
