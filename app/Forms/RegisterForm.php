<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class RegisterForm extends Form
{
    protected $formOptions = [
        'method' => 'POST',
        'class' => 'form form-auth-register'
    ];

    protected $clientValidationEnabled = true;

    public function buildForm() {

        $this->add('username', 'text', [
            'attr' => ['class' => 'input'],
            'label' => __('Username'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-user-circle'],
            'rules' => 'required'
        ])
        ->add('email', 'email', [
            'attr' => ['class' => 'input'],
            'label' => __('Email'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-envelope-square'],
            'rules' => 'required'
        ])
        ->add('password', 'password', [
            'attr' => ['class' => 'input'],
            'label' => __('Password'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-key'],
            'rules' => 'required'
        ])
        ->add('password_confirmation', 'password', [
            'attr' => ['class' => 'input'],
            'label' => __('Confirm password'),
            'label_attr' => ['class' => 'label'],
            'icon' => ['class' => 'fas fa-key']
        ])
        ->add('recaptcha', 'grecaptcha', [
            'label_show' => false,
            'value' => $this->getData('captchaWidget'),
        ])
        ->add('group', 'buttongroup', [
            'wrapper' => ['class' => 'field is-grouped'],
            'buttons' => [
                [
                    'label' => '<span class="icon"><i class="fas fa-check"></i></span><span>'.__('Register').'</span>',
                    'attr' => ['class' => 'button is-link', 'type' => 'submit']
                ],
                [
                    'label' => '<span class="icon"><i class="fas fa-undo-alt"></i></span><span>'.__('Reset').'</span>',
                    'attr' => ['class' => 'button is-light', 'type' => 'reset']
                ]
            ]
        ]);
    }
}
