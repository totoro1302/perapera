<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class LoginForm extends Form
{
    protected $formOptions = [
        'method' => 'POST',
        'class' => 'form form-auth-login'
    ];

    protected $clientValidationEnabled = true;

    public function buildForm() {

        $this->add('email', 'email', [
            'attr' => ['class' => 'input'],
            'label' => __('Email'),
            'label_attr' => ['class' => 'label'],
            'rules' => 'required|email',
            'icon' => ['class' => 'fas fa-envelope']
        ])
        ->add('password', 'password', [
            'attr' => ['class' => 'input'],
            'label' => __('Password'),
            'label_attr' => ['class' => 'label'],
            'rules' => 'required|string',
            'icon' => ['class' => 'fas fa-key']
        ])
        ->add('remember', 'checkbox', [
            'wrapper' => ['class' => 'field'],
            'label' => __('Remember me'),
            'label_attr' => ['class' => 'checkbox'],
        ])
        ->add('group', 'buttongroup', [
                'wrapper' => ['class' => 'field is-grouped'],
                'buttons' => [
                    [
                        'label' => '<span class="icon"><i class="fas fa-paper-plane"></i></span><span>'.__('Sign In').'</span>',
                        'attr' => ['class' => 'button is-link', 'type' => 'submit']
                    ],
                    [
                        'label' => '<span class="icon"><i class="fas fa-undo-alt"></i></span><span>'.__('Reset').'</span>',
                        'attr' => ['class' => 'button is-light', 'type' => 'reset']
                    ]
                ]
        ])
        ->add('password.request', 'static', [
            'wrapper' => ['class' => 'field is-grouped is-grouped-right'],
            'label_show' => false,
            'tag' => 'a',
            'attr' => ['class' => 'button is-text is-small', 'href' => route('password.request')],
            'value' => __('Forgot Your Password?')
        ]);
    }
}
