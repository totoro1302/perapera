<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ForgotPasswordForm extends Form
{
    protected $formOptions = [
        'method' => 'POST',
        'class' => 'form form-auth-forgotpassword'
    ];

    protected $clientValidationEnabled = true;

    public function buildForm()
    {
        $this->add('email', 'email', [
            'attr' => ['class' => 'input'],
            'wrapper' => ['class' => 'field'],
            'label' => __('Email'),
            'label_attr' => ['class' => 'label'],
            'rules' => 'required|email',
            'icon' => ['class' => 'fas fa-envelope']
        ])
        ->add('recaptcha', 'grecaptcha', [
            'label_show' => false,
            'value' => $this->getData('captchaWidget'),
        ])
        ->add('submit', 'submit', [
            'attr' => ['class' => 'button is-link'],
            'wrapper' => ['class' => 'field'],
            'label' => '<span class="icon"><i class="fas fa-paper-plane"></i></span><span>'.__('Send Password Reset Link').'</span>'
        ]);
    }
}
