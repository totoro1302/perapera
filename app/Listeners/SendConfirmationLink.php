<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\RegisterSuccess;
use App\Models\AccountConfirmation;

class SendConfirmationLink
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $token = hash('gost', mt_rand().$event->user->username);

        AccountConfirmation::create([
            'email' => $event->user->email,
            'token' => $token
        ]);

        $event->user->notify(new RegisterSuccess($token));
    }
}
