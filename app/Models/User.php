<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Identicon\Identicon;
use Identicon\Generator\SvgGenerator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function generateAvatar(string $name) : string {

        $filename = Carbon::today()->year . DIRECTORY_SEPARATOR;
        $filename .= Carbon::today()->month . DIRECTORY_SEPARATOR;
        $filename .= hash('gost', $name) . '.svg';

        $identicon = new Identicon(new SvgGenerator());
        $imageData = $identicon->getImageData($name, 512, rand_hexa_color());

        Storage::put($filename, $imageData);

        $this->avatar = $filename;

        return $this;
    }
}
