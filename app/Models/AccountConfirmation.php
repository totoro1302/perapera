<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class AccountConfirmation extends Model
{
    use SoftDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['email', 'token'];

    public static function enableAccount(string $token) {

        DB::transaction(function() use ($token) {
            $accountConfirmation = self::where('token', $token)->firstOrFail();

            $user = User::where('email', $accountConfirmation->email)->firstOrFail();
            $user->enabled = true;
            $user->save();

            $accountConfirmation->delete();
        });
    }
}
