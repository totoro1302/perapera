<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Forms\ForgotPasswordForm;
use Totoro1302\Captcha\CaptchaContract;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails, FormBuilderTrait;

    /**
     * Display the form to request a password reset link.
     *
     * @param CaptchaContract $captcha
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLinkRequestForm(CaptchaContract $captcha)
    {
        $form = $this->form(ForgotPasswordForm::class, [
            'url' => route('password.email')
        ], ['captchaWidget' => $captcha->getWidget()]);

        return view('auth.passwords.email', compact('form', 'captcha'));
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'g-recaptcha-response' => 'required|captcha'
        ]);
    }
}
