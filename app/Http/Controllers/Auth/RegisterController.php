<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Forms\RegisterForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Totoro1302\Captcha\CaptchaContract;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Models\AccountConfirmation;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, FormBuilderTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @param CaptchaContract $captcha
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm(CaptchaContract $captcha)
    {
        $form = $this->form(RegisterForm::class, [
            'url' => route('register')
        ], ['captchaWidget' => $captcha->getWidget()]);

        return view('auth.register', compact('form', 'captcha'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:100|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:10|confirmed',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with([
                'notification' => [
                    'type' => 'is-success',
                    'icon' => 'far fa-thumbs-up',
                    'message' => sprintf(__('Registration successful! An email has been sent to %s to activate your account.'), $user->email)
                ]
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = new User();

        $user->fill([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $user->generateAvatar($data['username']);

        $user->save();

        return $user;
    }

    /**
     * Enable a newly registered user
     *
     * @param string $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function accountConfirmation(string $token) {

        AccountConfirmation::enableAccount($token);

        return redirect('login')->with([
            'notification' => [
                'type' => 'is-success',
                'icon' => 'far fa-thumbs-up',
                'message' => __('Your account is now enable and you can sign in!')
            ]
        ]);
    }
}
