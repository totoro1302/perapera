<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GenerateVusJsInitialData
{

    protected $_view;

    public function __construct(ViewFactory $view){
        $this->_view = $view;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // VusJs components (notification, navbar) need some initial datas that are generated PHP side
        $vueJsData = [
            'navbar' => [
                'home' => [
                    'label' => config('app.name'),
                    'url' => route('home'),
                ]
            ],
            'notification' => []
        ];

        if(Auth::check() === true){
            $vueJsData['navbar'] += [
                'authUser' => [
                    'username' => Auth::user()->username,
                    'avatar' => Storage::disk('public')->url(Auth::user()->avatar),
                    'profile' => [
                        'label' => __('Profile'),
                        'url' => 'test',
                        'icon' => 'fas fa-user-edit',
                    ],
                    'settings' => [
                        'label' => __('Settings'),
                        'url' => 'test',
                        'icon' => 'fas fa-user-cog'
                    ],
                    'logout' => [
                        'label' => __('Logout'),
                        'url' => route('logout'),
                        'icon' => 'fas fa-sign-out-alt',
                        'csrf' => csrf_token()
                    ]
                ]
            ];
        } else {
            $vueJsData['navbar'] += [
                'unauthUser' => [
                    'register' => [
                        'label' => __('Register'),
                        'url' => route('register'),
                        'icon' => 'fas fa-pencil-alt'
                    ],
                    'login' => [
                        'label' => __('Login'),
                        'url' => route('login'),
                        'icon' => 'fas fa-sign-in-alt'
                    ]
                ]
            ];
        }

        if($request->session()->has('notification')){
            $vueJsData['notification'] = [
                'message' => $request->session()->get('notification')['message'],
                'type' => $request->session()->get('notification')['type'] ?? '',
                'icon' => $request->session()->get('notification')['icon'] ?? '',
            ];
        } else {
            $vueJsData['notification'] = [
                'message' => 'Salut!!! Ceci est un message de test.',
                'type' => 'is-info',
                'icon' => 'fab fa-android',
            ];
        }

        //We share a global variable make it available for all views
        $this->_view->share('vueJsData', $vueJsData);

        return $next($request);
    }
}
