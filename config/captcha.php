<?php

return [

    /*
     *--------------------------------
     * Google ReCaptcha
     *--------------------------------
     * Private key & Site Key mandatory if you are using Google ReCaptcha
     *
     */

    'recaptcha_secretkey' => env('RECAPTCHA_SECRETKEY', 'abcbdefghijklmnopqrstuvwxyz0123456789'),

    'recaptcha_sitekey' => env('RECAPTCHA_SITEKEY', 'abcbdefghijklmnopqrstuvwxyz0123456789'),

    'recaptcha_type' => env('RECAPTCHA_TYPE', 'googlerecaptchav2')
];