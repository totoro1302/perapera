import Vue from 'vue';
import NotificationComponent from './components/NotificationComponent';
import ChannelComponent from './components/ChannelComponent';
import InputComponent from './components/InputComponent';
import DialogComponent from './components/DialogComponent';
import NavbarComponent from './components/NavbarComponent';
import ModalComponent from './components/ModalComponent';
import {ScrollBar} from './simplebar';

new Vue({
    el: '#app',
    components: {
        NotificationComponent,
        ChannelComponent,
        InputComponent,
        DialogComponent,
        NavbarComponent,
        ModalComponent
    },
    data: {
        navbarData: vueJsData.navbar,
        notificationData: vueJsData.notification
    }
});

let myScrollBars = new ScrollBar([
    {cssId:'channel-wrapper',scrollToBottom:false},
    {cssId:'dialog-wrapper',scrollToBottom:true},
]);

myScrollBars.initialize();

