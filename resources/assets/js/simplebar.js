import SimpleBar from "simplebar";

// let simpleBarChannel = new SimpleBar(document.getElementById('channel-wrapper'));
// let simpleBarDialog = new SimpleBar(document.getElementById('dialog-wrapper'));
//
// let scrollElementDialog = simpleBarDialog.getScrollElement();
// scrollElementDialog.scrollTop = scrollElementDialog.scrollHeight - scrollElementDialog.clientHeight;
// simpleBarDialog.getScrollElement().addEventListener('scroll', function(e){});

export class ScrollBar {

    /**
     * elmtArray parameter is an array with the format [{ cssId: 'first', scrollToBottom: true }, ...]
     * @param elmtArray
     */
    constructor(elmtArray = []){
        this.elmtArray = elmtArray;
        this.scrollbarInstanceArray = [];
    }

    _triggerScrollBar(){
        this.elmtArray.forEach(elmt => {
            this.scrollbarInstanceArray.push(new SimpleBar(document.getElementById(elmt.cssId)));
        });
    }

    _scrollToBottom(){
        this.scrollbarInstanceArray.forEach((obj, i) => {
            if(this.elmtArray[i].scrollToBottom === true){
                let scrollElmt = obj.getScrollElement();
                scrollElmt.scrollTop = scrollElmt.scrollHeight - scrollElmt.clientHeight;
            }
        });
    }

    _onScrollEvent(e){

    }

    initialize(){
        this._triggerScrollBar();
        this._scrollToBottom();
    }
}