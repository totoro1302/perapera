<header>
    <notification-component :notification="notificationData"></notification-component>
    <navbar-component :menu="navbarData"></navbar-component>
</header>