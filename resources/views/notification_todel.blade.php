@if(session('notification.message'))
    <div class="notification is-radiusless is-marginless {{ session('notification.type') }}">
        <button class="delete"></button>
        <p class="has-text-centered">
            @if(session('notification.icon'))
                <span class="icon is-large"><i class="fa-lg {{ session('notification.icon') }}"></i></span>
            @endif
            {{ session('notification.message') }}
        </p>
    </div>
@endif
<notification-component :notification="{message: '{{ session('notification.message') }}', icon: '{{ session('notification.icon') }}', type: '{{session('notification.type') }}' }"></notification-component>