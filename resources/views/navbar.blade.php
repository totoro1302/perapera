<nav>
    <a class="brand" href="{{ route('home') }}">{{ config('app.name') }}</a>

    @auth
        <a href="#" class="account">
            <figure class="avatar">
                <img src="{{ Storage::disk('public')->url(Auth::user()->avatar) }}">
            </figure>
            <span>{{ Auth::user()->username }}&nbsp;<i class="fas fa-angle-down"></i></span>
        </a>
    @endauth
    @guest
        <a href="{{ route('register') }}">
            <span class="icon"><i class="fas fa-pencil-alt"></i></span><span>{{ __('Register') }}</span>
        </a>
        <a href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i><span class="label">{{ __('Login') }}</span></a>
    @endguest
</nav>