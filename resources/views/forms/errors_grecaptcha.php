<?php if (isset($errors)): ?>
    <?php foreach ($errors->get('g-recaptcha-response') as $err): ?>
        <div <?= $options['errorAttrs'] ?>><?= $err ?></div>
    <?php endforeach; ?>
<?php endif; ?>

