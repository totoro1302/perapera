<?php if ($options['wrapper'] !== false): ?>
<div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>

    <?php foreach($options['buttons'] as $button): ?>
        <div class="control">
            <?= Form::button($button['label'], $button['attr']) ?>
        </div>
    <?php endforeach; ?>


    <?php if ($options['wrapper'] !== false): ?>
</div>
<?php endif; ?>

