<?php if ($showLabel && $showField): ?>
<?php if ($options['wrapper'] !== false): ?>
<div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
    <?php endif; ?>

    <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
        <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
    <?php endif; ?>

    <?php if ($showField): ?>
        <div class="control <?php echo isset($options['icon']) ? 'has-icons-left has-icons-right' : ''; ?>">

            <?php if ($showError && $errors->has($name)) {
                $options['attr']['class'] .= ' is-danger';
            } ?>

            <?= Form::input($type, $name, $options['value'], $options['attr']) ?>
            <?php if (isset($options['icon'])){ ?>
                <span class="icon is-left <?php echo $options['icon']['size'] ?? 'is-small'; ?>">
                <i class="<?php echo ($options['icon']['class']) ?? ''; ?>"></i>
            </span>
            <?php } ?>

            <?php if ($showError && $errors->has($name)) { ?>
                <span class="icon is-right <?php echo $options['icon']['size'] ?? 'is-small'; ?>">
                <i class="fas fa-exclamation-triangle"></i>
            </span>
            <?php } ?>

        </div>
        <?php include 'help_block.php' ?>
    <?php endif; ?>

    <?php include 'errors.php' ?>

    <?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
</div>
<?php endif; ?>
<?php endif; ?>

