@extends('layouts.app')

@section('title', __('Homepage'))

@section('content')


    @auth
        <div class="messaging-wrapper">
            <div class="channel-wrapper">
                <channel-component panel-title="{{ __('My Channels') }}"></channel-component>
            </div>
            <div class="dialog-wrapper">
                <dialog-component></dialog-component>
                <input-component></input-component>
            </div>
        </div>
    @elseauth
    @endauth

@endsection


