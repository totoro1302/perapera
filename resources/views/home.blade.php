@extends('layouts.app')

@section('title', __('Homepage'))

@section('content')
    <div class="left" id="channel-wrapper">
        <channel-component></channel-component>
    </div>
    <div class="middle">
        <div class="middle-top" id="dialog-wrapper">
            <dialog-component></dialog-component>
        </div>
        <div class="middle-bottom"><textarea class="textarea is-medium" rows="2" placeholder="Type"></textarea></div>
    </div>
    <div class="right">Another sidebar</div>
    <modal-component></modal-component>
@endsection