<nav class="navbar is-dark" role="navigation" aria-label="main navigation">

    <div class="navbar-brand">
        <a class="navbar-item" href="{{ route('home') }}">{{ config('app.name') }}</a>
        <div class="navbar-burger">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div class="navbar-menu">
        <div class="navbar-end">
            @auth
                <div class="navbar-item has-dropdown is-hoverable">
                    <span class="navbar-link">
                        <figure class="image is-24x24 avatar">
                            <img src="{{ Storage::disk('public')->url(Auth::user()->avatar) }}">
                        </figure>&nbsp;
                        <span>{{ Auth::user()->username }}</span>
                    </span>
                    <div class="navbar-dropdown is-right">
                        <a class="navbar-item">
                            <span class="icon"><i class="fas fa-fw fa-user-circle"></i></span><span>{{ __('My profile') }}</span>
                        </a>
                        <a class="navbar-item">
                            <span class="icon"><i class="fas fa-fw fa-cogs"></i></span><span>{{ __('My settings')  }}</span>
                        </a>
                        <a class="navbar-item">
                            <span class="icon"><i class="fas fa-fw fa-envelope"></i></span><span>{{ __('My messages') }}</span>
                        </a>
                        <hr class="navbar-divider">
                        <a class="navbar-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <span class="icon"><i class="fas fa-fw fa-sign-out-alt"></i></span><span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    </div>
                </div>
            @endauth
            @guest
                <a class="navbar-item" href="{{ route('register') }}"><span class="icon"><i class="fas fa-fw fa-pencil-alt"></i></span><span>{{ __('Register') }}</span></a>
                <a class="navbar-item" href="{{ route('login') }}"><span class="icon"><i class="fas fa-fw fa-sign-in-alt"></i></span><span>{{ __('Login') }}</span></a>
            @endguest
        </div>
    </div>
</nav>