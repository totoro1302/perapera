<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
        <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

        <!-- Styles -->
        <link rel="manifest" href="{{ asset('manifest.json') }}">
        <link href="{{ asset_manifest('css/app.css') }}" rel="stylesheet">
        @stack('css')

        <!-- Scripts Head -->
        @stack('jshead')
        <script>
            let vueJsData = @json($vueJsData);

            @stack('jsheadinline')
        </script>
    </head>
    <body>

        <div id="app" class="app">

            @component('header')
            @endcomponent

            <section class="main">
                @yield('content')
            </section>

        </div>

        <script src="{{ asset_manifest('js/app.js') }}"></script>
        @stack('jsbody')

        <script>
            @stack('jsheadinline')
        </script>
    </body>
</html>