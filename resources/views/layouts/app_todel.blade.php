<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
        <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

        <!-- Styles -->
        <link rel="manifest" href="{{ asset('manifest.json') }}">
        <link href="{{ asset_manifest('css/app.css') }}" rel="stylesheet">
        @stack('css')

        <!-- Scripts Head -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        @stack('jshead')
    </head>
    <body>
        <div id="app">
            @component('notification')
            @endcomponent

            @component('navbar')
            @endcomponent

            <section class="section">
                <div class="container is-fullhd">
                    @yield('content')
                </div>
            </section>

        </div>
        <script src="{{ asset_manifest('js/app.js') }}"></script>
        @stack('jsbody')
    </body>
</html>
