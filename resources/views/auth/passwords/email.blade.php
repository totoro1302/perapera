@extends('layouts.app')

@section('title', __('Forgot your password'))

@push('jshead')
    <script src="{{ $captcha->getGoogleRecaptchaJsApi() }}"></script>
@endpush

@section('content')
    <div class="form-container">
        {!! form($form) !!}
    </div>
@endsection