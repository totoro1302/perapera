@extends('layouts.app')

@section('title', __('Reset your password'))

@section('content')
    <div class="form-container">
        {!! form($form) !!}
    </div>
@endsection