@extends('layouts.app')

@section('title', __('Login'))

@section('content')
    <div class="form-container">
        {!! form($form) !!}
    </div>
@endsection