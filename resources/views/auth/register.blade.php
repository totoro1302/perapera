@extends('layouts.app')

@section('title', __('Register'))

@push('jshead')
    <script src="{{ $captcha->getGoogleRecaptchaJsApi() }}"></script>
@endpush

@section('content')
    <div class="form-container">
        {!! form($form) !!}
    </div>
@endsection