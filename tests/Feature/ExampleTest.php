<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    public function testBasicTestTwo()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function testBasicTestThree()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
