const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ManifestPlugin = require('webpack-manifest-plugin');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    mode: 'development',
    entry: { app: ['./resources/assets/sass/app.scss', './resources/assets/js/app.js'] },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/[name].js'
    },
    resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: { 'vue$': 'vue/dist/vue.esm.js' }
    },
    devtool: 'inline-source-map',
    optimization: {
        minimize: false,
    },
    watch: true,
    module: {
        rules: [
            {
                test:/\.js$/,
                exclude: /(node_modules|bower_components)/,
                include: path.resolve(__dirname, 'resources/assets/js')
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'vue-style-loader' },
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: 'css-loader', options: { importLoaders: 1, minimize: false }},
                    { loader: 'postcss-loader', options: { plugins: (loader) => [ require('autoprefixer')({browsers: ['last 2 versions', 'ie >= 7']}),]}},
                    { loader: 'sass-loader'}
                ],
            },
            {
                test: /\.css$/,
                use:[ MiniCssExtractPlugin.loader, 'css-loader' ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    { loader: 'file-loader', options: {
                            regExp: /[\/\\]([a-zA-Z0-9-]+)[\/\\]([a-zA-Z0-9-]+)\.(eot|svg|ttf|woff|woff2)$/,
                            name: '[1]/[name].[ext]',
                            outputPath: 'fonts/',
                            publicPath: '../fonts'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ManifestPlugin({
            map: (obj) => {
                const extension = obj.name.split('.').pop();
                if(extension === 'map'){
                    obj.name = 'js/' + obj.name;
                } else {
                    obj.name = extension + '/' + obj.name;
                }
                return obj;
            }
        }),
        new MiniCssExtractPlugin({filename: 'css/[name].css', chunkFilename: 'css/[id].css'}),
        new VueLoaderPlugin()
    ]
};